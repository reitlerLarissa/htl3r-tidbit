<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		return View::make('hello');
	}

	public function showStart()
	{
		$tidbits = Tidbit::where('id', '>', 0)->get();
		return View::make('tidbits', array('tidbits' => $tidbits));
	}

	public function goToDatabase(){
		$tidbit = new Tidbit(); 
		$tidbit->bild = Input::get('bild');
		$tidbit->titel = Input::get('titelhin');
		$tidbit->text = Input::get('text');

		$tidbit->save();

		/*DB::table('tidbits')->insert(
			array('bild' => Input::get('bild'),
				  'titel' => Input::get('titel'),
				  'text' => Input::get('text')));
		*/
		return Redirect::back();
	}

	public function delData(){
		$tidbit;
		if($tidbit->titel == Input::get('titelweg')){
			$tidbit->titel = delete('titelweg');
		}			

		$tidbit->save();
		return Redirect::back();
	}

}