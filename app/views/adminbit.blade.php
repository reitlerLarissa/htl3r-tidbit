<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tidbit</title>
    <style>

        body{
            background-color: #ccaabb;
            background-image: url("../lari.png");
        }
        .box{
            padding-left: 20px;
            padding-top: -10px;
            margin: auto;
            margin-top: 30px;
            width: 300px;
            height: 100px;
            border: 5px solid white;
            background-color: #cc88aa;
            font-size: 13px;
        }
        .bild{
            width: 200px;
            height: 200px;
            margin-top: 25px;
            margin-left: 25px;
            float: left;
            margin-right: 25px;
        }
        .box h2{
            font-family: Comic Sans MS;
            float: left;
        }
        .box p{
            font-family: Comic Sans MS;
            clear: left;
            float: left;
        }
        .butt{
            margin-right: 20px;
            float: right;
            margin-bottom: 20px;
        }


    </style>
</head>
<body>
	@foreach ($tidbits as $tidbit)
        <div class="box">
            <h2>{{ $tidbit->titel }}</h2>
            <p>{{ $tidbit->text }}</p>

            <form action="/Tidbit/Code/Tidbit/public/tidbit/delete" method='post'>
                <input type="hidden" name="id" value={{$tidbit->id}}>
                <input class="butt" type="submit" value="wegdamit" />
            </form>
        </div>
	@endforeach
  
</body>
</html>