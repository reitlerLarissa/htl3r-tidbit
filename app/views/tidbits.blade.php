<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tidbit</title>
    <style>

        body{
            background-color: #ccaabb;
            background-image: url("../lari.png");
        }
        .box{
            margin: auto;
            margin-top: 30px;
            width: 700px;
            height: 250px;
            border: 5px solid white;
            background-color: #cc88aa;
        }
        .bild{
            width: 200px;
            height: 200px;
            margin-top: 25px;
            margin-left: 25px;
            float: left;
            margin-right: 25px;
        }
        .box h2, .box p{
            font-family: Comic Sans MS;
        }
        .box img{
            border: 3px solid #ccaabb;
            background-color: white;
        }

    </style>
</head>
<body>
	@foreach ($tidbits as $tidbit)
        <div class="box">
            <img class="bild" src="{{ $tidbit->bild }}" />
            <h2>{{ $tidbit->titel }}</h2>
            <p>{{ $tidbit->text }}</p>
        </div>
	@endforeach

<form action="/Tidbit/Code/Tidbit/public/tidbit/create" method='post'>
    <label>Bild</label>
    <input type="text" name="bild" value="" />
    <br />
    <label>Titel hinzufügen</label>
    <input type="text" name="titelhin" value="" />
    <br />
    <label>Text</label>
    <input type="text" name="text" value="" />
    <br />
    <input type="submit" value="herdamit" />
</form>
<form action="/Tidbit/Code/Tidbit/public/tidbit/delete" method='post'>
    <label>Titel entfernen</label>
    <input type="text" name="titelweg" value="" />
    <br />
    <input type="submit" value="wegdamit" /> 
</form>   
</body>
</html>
